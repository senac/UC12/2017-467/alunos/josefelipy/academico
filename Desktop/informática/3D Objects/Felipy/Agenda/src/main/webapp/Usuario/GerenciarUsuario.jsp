<%@page import="java.util.List"%>
<%@page import="br.com.senac.agenda.model.Usuario"%>
<jsp:include page="../Header.jsp"/>
  
<% String salvar = (String) request.getAttribute ("salvar") ; %>



<% if (salvar != null) { %>
<div class=" alert alert-success ">
    
    <%= salvar %>
    
    </div>
    
    <% } %>

    
   <fieldset>
       

       <legend>Pesquisa de Usu�rio</legend>
    
       <form  action="./SalvarUsuarioServlet">
        <div class="form-group">
            <label for="textCodigo" style="margin-right: 10px">C�digo</label> <input name="codigo" class="form-control col-1" id="txtCodigo" type="text" readonly/>

        </div>
        <div class="form-group">
            <label for="textNome" style="margin-right: 10px">Nome</label> <input id="id" name="nome" class="form-control form-control-sm" id="txtNome" type="text" />
        </div>
        <div class="form-group">
            <label for="textSenha" style="margin-right: 10px">Senha</label> <input name="senha" class="form-control form-control-sm" id="txtSenha" type="text" />
        </div>
        <div>
        <button type="submit" class="btn btn-success" style="margin-left: 10px">Salvar</button>
        <button type="reset" class="btn btn-danger" style="margin-left: 10px">Cancelar</button>
        </div>

    </form>

</fieldset>

        
<jsp:include page="../Footer.jsp"/>