/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.agenda.dao;



import br.com.senac.agenda.model.Contato;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

public class ContatoDAO extends DAO<Contato> {

    @Override
    public void salvar(Contato contato) {

        Connection connection = null;

        try {

            String query;

            if (contato.getCodigo() == 0) {
                query = "INSERT INTO contato (nome , telefone, celular, cep, endereco, numero, bairro, cidade, estado, email) values (? , ? , ? , ? , ? , ? , ? ,? , ? , ? );";

            } else {
                query = "update contato set nome = ? , telefone = ? , celular = ? , cep = ? , endereco = ?, numero = ? , bairro = ? , cidade = ? , estado = ? , email = ? where id = ? ;";
            }
            connection = Conexao.getConnection();
            PreparedStatement ps = connection.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);
            ps.setString(1, contato.getNome());
            ps.setString(2, contato.getTelefone());
            ps.setString(3, contato.getCelular());
            ps.setString(4, contato.getCep());
            ps.setString(5, contato.getEndereco());
            ps.setString(6, contato.getNumero());
            ps.setString(7, contato.getBairro());
            ps.setString(8, contato.getCidade());
            ps.setString(9, contato.getEstado());
            ps.setString(10, contato.getEmail());

            if (contato.getCodigo() == 0) {
                ps.executeUpdate();
                ResultSet rs = ps.getGeneratedKeys();
                rs.first();
                contato.setCodigo(rs.getInt(1));
            } else {
                ps.setInt(11, contato.getCodigo());
                ps.executeUpdate();
            }

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("FALHAAAAAAAAAAAAA");
        } finally {
            try {
                connection.close();
            } catch (Exception e) {
                System.out.println("Erro de Conexao");
            }
        }

    }

    @Override
    public void deletar(Contato objeto) {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    @Override
    public List<Contato> listar() {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    @Override
    public Contato get(int id) {
        throw new UnsupportedOperationException("Not supported yet."); 
    }

}