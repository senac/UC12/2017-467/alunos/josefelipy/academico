/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.servlet;

import br.com.senac.agenda.dao.UsuarioDAO;
import br.com.senac.agenda.model.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author sala302b
 */
@WebServlet(name = "PesquisaUsuarioServlet", urlPatterns = {"/Usuario/PesquisaUsuarioServlet"})
public class PesquisaUsuarioServlet extends HttpServlet {

  
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    String mensagem = null;
             
            
            try {
        String nome = request.getParameter("nome");
        String codigo = request.getParameter("codigo") ; 
        Integer id = null ; 
        
        if( codigo != null && !codigo.trim().isEmpty()){
            id = new Integer(codigo);
        }
        
        UsuarioDAO dao = new UsuarioDAO();
        
        List<Usuario> lista = dao.getByFiltro(id, nome);
        
        request.setAttribute("lista", lista);
        
        
        }catch (NumberFormatException ex){
            mensagem = "Campo codigo somento aceita valores numericos." ;
            request.setAttribute("mensagem" , mensagem);
 }

        
        RequestDispatcher dispatcher =  request.getRequestDispatcher("./PesquisaUsuario.jsp");
        
        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       processRequest(req, resp); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public String getServletInfo() {
        return "Short description" ;
    }
    

}

