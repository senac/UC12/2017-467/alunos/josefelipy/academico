<%@page  import="java.util.List"%>
<%@page  import="br.com.senac.agenda.model.Contato"%>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

<jsp:include page="../Header.jsp"/>

<% List<Contato> lista = (List) request.getAttribute("lista");%>
<% String mensagem = (String) request.getAttribute ("mensagem");%>


<% if (mensagem != null) {%>

<div class="alert alert-danger">
    
    <%= mensagem %>
    
</div>
    
    <% } %>
   
    <fieldset>
        
        <legend>Pesquisa de Contatos</legend>
        <form class="form-inline" action="./PesquisaContatoServlet">
            <div class="form-group" style="padding: 20px">
                <label for="textCodigo" style="margin-right: 10px">C�digo: </label>
                <input id="id" name="codigo" class="form-control form-control-sm" id="textCodigo" type="text" />
            </div>
              
            <div class="form-group">
                <label for="textNome" style="margin-right: 10px">Nome: </label>
                <input id="nome" name="nome" class="form-control form-control-sm" id="textNome" type="text" />
            </div>
            <div class="form-group col-4">
            <label for="textEstado">Estado:</label>
            <select type="text" id="estado" name="estado" class="form-control">
                <option value="" selected>--</option>
                <option value="ES" >ES</option>
                <option value="SP" >SP</option>
                <option value="RJ" >RJ</option>
                <option value="MG" >MG</option>
                <option value="BH" >BH</option>
                <option value="UF" >UF</option>
            </select>
        </div> 
            <button type="submit" class="btn btn-dark"" style="margin-left: 2px"> <i class="fa fa-user" aria-hidden="true"></i> Pesquisar</button> 
        <a href="./CadastroContato.jsp" class="btn btn-success" style="margin-left: 2px" > <i class="fa fa-user-plus" aria-hidden="true"></i> Adicionar  <a/> 
        </form>
    </fieldset>
    <hr/>
    <table class="table table-hover table-dark">
        <thead>
            <tr class="bg-success">
                <!-- Arrumar a ordem dos nomes em sala. -->
                <th>C�digo</th> <th>Nome</th> <th>Telefone</th> <th>Celular</th> <th>Endere�o</th> <th>Email</th>  <th>Editar </th>
                
            </tr>
            
        </thead>
        <% if (lista != null && lista.size() > 0) {
            for (Contato c : lista){
        
        %>
        <tr>
            <td><%= c.getCodigo() %></td><td><%= c.getNome() %></td><td><%= c.getTelefone()%></td><td><%= c.getCelular() %></td><td><%= c.getEnderecoCompleto() %></td><td><%= c.getEmail()%></td>
            
            
            <td style="width: 120px">
           <a href="./CadastroContatoServlet?codigo=<%= c.getCodigo()%>">
                <img src="../resources/imagens/Edit.png">
            </a>
                <a  data-toggle="modal" data-target="#exampleModal" onclick="deletar(<%=c.getCodigo()%> ,'<%=c.getNome() %>' );">
                <img src="../resources/imagens/Delete.png">
            </a>
           
                
                
            </td>
        </tr>
        <% } 
        
} else { %>

<tr>
    <td colspan="2">N�o existem registros.</td>
</tr>
<%}%>
    </table>
            
            
            
        
<jsp:include page="../Footer.jsp"/>